---
author: "Steve Klabnik and Carol Nichols"
date: 2022-02-24
linktitle: Перечисления и сопоставление с образцом
menu:
main:
parent: tutorials
next: /tutorials/github-pages-blog
prev: /tutorials/automated-deployments
title: 6 Перечисления и сопоставление с образцом
categories: 06 enums
tags:
- rust
---

В этой главе мы рассмотрим *перечисления (enumerations)*, также называемые *enums*. Перечисления позволяют определять
типы путём перечисления их возможных *вариантов*. Во-первых, мы определим и используем перечисление, чтобы показать, как
оно может объединить значения и данные. Далее мы рассмотрим особенно полезное перечисление `Option`, которое указывает,
что значение может быть или чем-то, или ничем. Затем мы посмотрим, как сопоставление шаблонов в выражении `match`
позволяет легко запускать разный код для разных значений перечислений. Наконец, мы узнаем, насколько
конструкция `if let` удобна и лаконична для обработки перечислений в вашем коде.
