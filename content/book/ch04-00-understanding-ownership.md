---
author: "Steve Klabnik and Carol Nichols"
date: 2022-02-24
linktitle: Понимание Владения
menu:
main:
parent: tutorials
next: /tutorials/github-pages-blog
prev: /tutorials/automated-deployments
title: 4 Понимание Владения
categories: 04 understanding ownership
tags:
- rust

---

Владение - это самая уникальная особенность Rust, которая имеет глубокие последствия для всего языка. Это позволяет Rust
обеспечивать безопасность памяти без использования сборщика мусора, поэтому важно понимать, как работает владение. В
этой главе мы поговорим о владении, а также о нескольких связанных с ним возможностях: заимствовании, срезах и о том,
как Rust раскладывает данные в памяти.
